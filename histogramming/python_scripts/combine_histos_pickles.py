# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 12:31:17 2019

@author: kelkar2
"""

import numpy as np
import pickle
import sys

input_folder = sys.argv[1]
lig_folder_prefix = sys.argv[2]
lig_folder_postfix = sys.argv[3]
pickle_prefix = sys.argv[4]
pickle_postfix = sys.argv[5]
num_trajs = int(sys.argv[6])
numx = int(sys.argv[7])
numy = int(sys.argv[8])
density_rep = sys.argv[9]
n = int(sys.argv[10])
m = 11
ligands = sys.argv[m:(m+n)]


for ligand in ligands:
    all_histos = np.zeros((1,numx,numy))
    for i in range(num_trajs):
#        try:
        ligand_input_folder = input_folder + lig_folder_prefix + ligand + lig_folder_postfix
        file_name = ligand_input_folder + pickle_prefix + str(i) + pickle_postfix
        print(file_name)
#        try:
        data = np.load(file_name)
        print('ligand -' + ligand + ', traj # ' + str(i) + ' loaded')
        all_histos = np.vstack((all_histos, np.reshape(data,(np.shape(data)[0],numx,numy))))
#        except:
#        print('ligand -' + ligand + ', traj # ' + str(i) + ' not found')
    pickle_out = open(input_folder + density_rep + ligand + '.pickle', 'wb')
    pickle.dump(all_histos, pickle_out)
    pickle_out.close()
