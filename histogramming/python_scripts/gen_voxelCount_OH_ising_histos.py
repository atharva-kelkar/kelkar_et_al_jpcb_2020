# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar (+ Reid Van Lehn)
@date: 04/03/2020

Python script for generating water density histograms for each frame in an MD trajectory.
Histograms are computed by using a Gaussian kernel function centered on each water molecule with
a tunable smearing parameter (larger = more diffuse kernel function)

Idea is to save these histograms as a pickle file for later post-processing to generate actual images. 
This is the most time-consuming part of the calculation.

"""

# Import libraries
from __future__ import print_function, division
import mdtraj as md
import numpy as np

from scipy import integrate
import sys
import math
import pickle
import time
from analysis_scripts.python_scripts.willard_chandler.wc_tools import find_wc_wc_arr_z_given_xy, read_wc

# Gaussian kernel using semaring parameter defined above
# Adjusted to take into account PBCs
def gaussian_kernel_function(s, scent, halfBoxVec):
    # take int account PBCs
    dist = s-scent
    if (dist > halfBoxVec):
        dist -= 2*halfBoxVec
    elif (dist < -1.0*halfBoxVec):
        dist += 2*halfBoxVec
    return kernel_prefix*math.exp(exponent_prefix*(dist)**2)

''' 
Function that takes the current indices, count variable, and number of atoms as input
    Returns new appended index array, incremented count variable
'''
def index_increment(ndx, ndx_curr, count, n):
    temp_ndx = ndx + count * n
    count += 1
#    print( ndx.shape )
#    print( ndx_curr.shape )
#    print( np.hstack((ndx_curr, temp_ndx)).shape )
    return np.hstack((ndx_curr, temp_ndx)), count
'''
Function takes coordinates, box dimensions, and index file
    Returns appended coordinates, appended indices
'''
def replicate_2D(coords, box_dims, ndx):
    
    num_atoms = coords.shape[0]
    count = 1
    
    coords_org = np.copy(coords)
    # + x
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0] 
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx, count, num_atoms)
    # -x 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0] 
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # +y 
    temp = np.copy(coords_org)
    temp[:, 1] += box_dims[1] 
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # -y
    temp = np.copy(coords_org)
    temp[:, 1] -= box_dims[1] 
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # +x,+y 
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0]
    temp[:, 1] += box_dims[1]
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # +x,-y 
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0]
    temp[:, 1] += -box_dims[1]
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # -x,-y 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0]
    temp[:, 1] += -box_dims[1]
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    # -x,+y 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0]
    temp[:, 1] += box_dims[1]
    coords = np.vstack((coords,temp))
    ndx_new, count = index_increment(ndx, ndx_new, count, num_atoms)
    
    return coords, ndx_new

'''
Function that histograms water molecules along the local Willard-Chandler surface at each point
    Returns histogram for given frame
'''
def plot_waters_along_wc(allWaterPos, cavity_dims, Ind, i, numx, numy, boxVec, wc_arr):
    allWaterPos, Ind_new = replicate_2D(allWaterPos, boxVec, Ind)
    allO = allWaterPos[Ind_new,:] # Positions of all water O atoms 
    dx = (cavity_dims[1, 0] - cavity_dims[0, 0])/numx
    dy = (cavity_dims[1, 1] - cavity_dims[0, 1])/numy
    dz = (cavity_dims[1, 2] - cavity_dims[0, 2])/2
    x = cavity_dims[0, 0]
    y = cavity_dims[0, 1]
    selected_water_molecules_mask = []
    for i in range(1, numx -1 ):
        for j in range(1, numy - 1 ):
            x_curr = x + dx * i
            y_curr = y + dy * j
            # Define z_curr given x_curr, y_curr and wc_arr
            z_curr = find_wc_wc_arr_z_given_xy( wc_arr, x_curr, y_curr, 2*dz )
            # Define x-mask name
            mask_name_x = 'mask_x'
            # Define executable string for x-mask filtering
            exec_string_x = mask_name_x + ' = (allO[:,0] >= (x_curr - dx) ) & (allO[:,0] <= (x_curr + dx) )'
            # Execute executable string
            exec( exec_string_x )
            # Define y-mask name
            mask_name_y = 'mask_y'
            # Define executable string for y-mask filtering
            exec_string_y = mask_name_y + ' = (allO[:,1] >= (y_curr - dy) ) & (allO[:,1] <= (y_curr + dy) )'
            # Execute executable string
            exec( exec_string_y )
            # Define z-mask name
            mask_name_z = 'mask_z'
            # Define executable string for z-mask filtering
            exec_string_z = mask_name_z + ' = (allO[:,2] >= (z_curr - dz) ) & (allO[:,2] <= (z_curr + dz) )'
            # Execute executable string
            exec( exec_string_z )
            # Define complete current mask
            temp_mask = eval(mask_name_x) & eval(mask_name_y) & eval(mask_name_z)
            # Specific i,j mask name
            i_j_mask_name = 'mask_x_' + str(i) + '_y_' + str(j)
            exec( i_j_mask_name + ' = temp_mask')
            selected_water_molecules_mask.append( temp_mask )
    selected_water_molecules_mask = np.array( selected_water_molecules_mask )
    
    allO_chosen = allO[ np.any(selected_water_molecules_mask, axis = 0) ] # Positions of water O atoms inside slab
    
    # Histogram in given range
    x_range = [cavity_dims[0,0], cavity_dims[1,0]]
    y_range = [cavity_dims[0,1], cavity_dims[1,1]]
    hist_range = [x_range, y_range]
    histo = np.histogram2d(allO_chosen[:,0], allO_chosen[:,1], (numx, numy), hist_range) 
    histo = histo[0].reshape((numx * numy))
    return histo
            
    
    
def plot_waters(allWaterPos, cavity_dims, Ind, i, numx, numy, boxVec):
    
    allWaterPos, Ind_new = replicate_2D(allWaterPos, boxVec, Ind)
    allO = allWaterPos[Ind_new,:] # Positions of all water O atoms 
    # Include water molecules within +- 0.2 nm (buffer) in x,y,z directions
    maskx_1 = (allO[:,0] >= cavity_dims[0,0]-buffer) & (allO[:,0] <= cavity_dims[1,0]+buffer)
    masky_1 = (allO[:,1] >= cavity_dims[0,1]-buffer) & (allO[:,1] <= cavity_dims[1,1]+buffer)
    maskz_1 = (allO[:,2] >= cavity_dims[0,2]) & (allO[:,2] <= cavity_dims[1,2])
    
    x_range = [cavity_dims[0,0], cavity_dims[1,0]]
    y_range = [cavity_dims[0,1], cavity_dims[1,1]]
    hist_range = [x_range, y_range]
    
    allO_chosen = allO[maskx_1 & masky_1 & maskz_1] # Positions of water O atoms inside slab
    
    histo = np.histogram2d(allO_chosen[:,0], allO_chosen[:,1], (numx, numy), hist_range) 
    histo = histo[0].reshape((numx * numy))
    return histo

# %% Import libraries, files, and data to be used in script, then execute   
if __name__ == "__main__":    
    # Read input parameters from command line    
    
    z = float(sys.argv[1]) # z value for center of slice
    dz = float(sys.argv[2]) # thickness of slice 
    sidelength = float(sys.argv[3]) # length of side for box
    numcolumns = int(sys.argv[4]) # number of bins in each dimension (columns = x, rows = y)
    numrows = int(sys.argv[5])
    input_top = sys.argv[6] # input files
    input_xtc = sys.argv[7] 
    output_prefix = sys.argv[8]
    layer = int(sys.argv[9])   # number of layer density to be calculated 
    translate_z = float(sys.argv[10]) # nm
    O_charge = float(sys.argv[11])
    H_charge = float(sys.argv[12])
    x_pos = float(sys.argv[13])
    y_pos = float(sys.argv[14])
    # variables needed for Gaussian kernel; width of smearing param in nm
    # 0.05 smearing function decays to 0 for +- 0.2 nm or so. 
    global smear, buffer, kernel_prefix, exponent_prefix
    smear = float(sys.argv[15])
    wc_file = sys.argv[16]
    end_group = sys.argv[17]
    place_along_wc_surface = float(sys.argv[18])
    
    buffer = 0.2 # nm
    int_buffer = 0.2 # nm
    
    # Normalized so that integral from -inf to inf is 1. 0.39... = 1/sqrt(2pi)
    kernel_prefix = 0.3989422804*1.0/smear
    exponent_prefix = -0.5*(1.0/(smear*smear))
    
    start_time = time.time()
    print("Input parameters: z = ", z, "; Thickness = ", dz, "; Side length = ", sidelength,"; Histograms: rows ", numrows, " by columns ", numcolumns)
    print("Loading topology ",input_top, "; Loading trajectory ",input_xtc)
    traj = md.load(input_xtc, top=input_top,stride=1)
    nFrames = int(traj.time.size)

    # get solvent atom indices
    ndxWater_O = traj.topology.select( 'water and type O' ) # find heavy water atoms
    ndxWater_H = traj.topology.select( 'water and type H' ) # find water H atoms
    
    # set up histogram parameters here, assuming a NVT simulation
    # first row is min, second row is max
    cavity_dims = np.zeros((2, 3), dtype=np.float)
    
    # get box vectors - assumes they are constant
    boxVec = traj.unitcell_lengths[0, :]
    
    # get center of box
    if ((x_pos == -1)):
        halfxVec = 0.5*boxVec[0]
        halfyVec = 0.5*boxVec[1]
    else:
        halfxVec = x_pos
        halfyVec = y_pos
    
    # z-dimensional length
    L = boxVec[2]
    
    # calculate bounds in all three axes for cavity we care about
    cavity_dims[0, 0] = halfxVec - sidelength*0.5
    cavity_dims[1, 0] = halfxVec + sidelength*0.5
    cavity_dims[0, 1] = halfyVec - sidelength*0.5
    cavity_dims[1, 1] = halfyVec + sidelength*0.5
    cavity_dims[0, 2] = z - dz*0.5
    cavity_dims[1, 2] = z + dz*0.5
    
    # set up histograms here, including bounds (to avoid excess calculations)
    # Will reset as needed
    xhistowidth = sidelength / numcolumns
    yhistowidth = sidelength / numrows
    
    # initialize all histograms here - will save to disk at end
    all_histos = np.zeros((nFrames, 2, numcolumns*numrows))
       
    wc_arr = read_wc( wc_file, 6 ) # Cutoff of 6 nm
    # Iterate over all frames and populate array

    for i in range(0, nFrames):
        #try:
        #print("Calculating for frame ", i)
        #except:
        #    print("Cannot print for {:d}".format(i))
        if place_along_wc_surface == 1:
            temp_O = plot_waters_along_wc(traj.xyz[i,:,:], cavity_dims, ndxWater_O, i, numrows, numcolumns, boxVec, wc_arr)
            temp_H = plot_waters_along_wc(traj.xyz[i,:,:], cavity_dims, ndxWater_H, i, numrows, numcolumns, boxVec, wc_arr)
        elif place_along_wc_surface == 0:
            temp_O = plot_waters(traj.xyz[i,:,:], cavity_dims, ndxWater_O, i, numrows, numcolumns, boxVec)
            temp_H = plot_waters(traj.xyz[i,:,:], cavity_dims, ndxWater_H, i, numrows, numcolumns, boxVec)
        all_histos[i, :] = np.vstack((temp_O, temp_H)) #+ temp_H*H_charge
    
    # save the raw histograms to disk
    output_file = open(output_prefix + '_' + str(layer) +  '_histos.pickle', "wb")
    # write pickle to disk, which can later be loaded for re-averaging, generating images, etc.
    pickle.dump(all_histos, output_file)
    output_file.close()
    end_time = np.round((time.time() - start_time)/60,1)
    print(  str(end_time) + 'mins')
