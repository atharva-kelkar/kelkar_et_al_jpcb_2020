# -*- coding: utf-8 -*-
"""
Python script for generating water density histograms for each frame in a MD trajectory.
Histograms are computed by using a Gaussian kernel function centered on each water molecule with
a tunable smearing parameter (larger = more diffuse kernel function)

Idea is to save these histograms as a pickle file for later post-processing to generate actual images. 
This is the most time-consuming part of the calculation.

"""

# Import libraries
from __future__ import print_function, division
import mdtraj as md
import numpy as np

from scipy import integrate
import sys
import math
import pickle
import time



# Gaussian kernel using semaring parameter defined above
# Adjusted to take into account PBCs
def gaussian_kernel_function(s, scent, halfBoxVec):
    # take int account PBCs
    dist = s-scent
    if (dist > halfBoxVec):
        dist -= 2*halfBoxVec
    elif (dist < -1.0*halfBoxVec):
        dist += 2*halfBoxVec
    return kernel_prefix*math.exp(exponent_prefix*(dist)**2)

def replicate_2D(coords, box_dims):
    
    coords_org = np.copy(coords)
    # + x
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0] 
    coords = np.vstack((coords,temp))
    # -x 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0] 
    coords = np.vstack((coords,temp))
    # +y 
    temp = np.copy(coords_org)
    temp[:, 1] += box_dims[1] 
    coords = np.vstack((coords,temp))
    # -y
    temp = np.copy(coords_org)
    temp[:, 1] -= box_dims[1] 
    coords = np.vstack((coords,temp))
    # +x,+y 
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0]
    temp[:, 1] += box_dims[1]
    coords = np.vstack((coords,temp))
    # +x,-y 
    temp = np.copy(coords_org)
    temp[:, 0] += box_dims[0]
    temp[:, 1] += -box_dims[1]
    coords = np.vstack((coords,temp))
    # -x,-y 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0]
    temp[:, 1] += -box_dims[1]
    coords = np.vstack((coords,temp))
    # -x,+y 
    temp = np.copy(coords_org)
    temp[:, 0] -= box_dims[0]
    temp[:, 1] += box_dims[1]
    coords = np.vstack((coords,temp))
    
    return coords



# Calculates histogram / image of solvent densities for a single snapshot and
# increments corresponding histograms
def calc_solvent_density(traj, ndxWater, framenum, numcolumns, numrows, xhistowidth, yhistowidth, halfxVec, halfyVec, cavity_dims, layer, translate_z, L, boxVec):
    # convert to positions 
    allWaterPos = traj.xyz[framenum, ndxWater, :]
    
    allWaterPos = replicate_2D(allWaterPos, boxVec)
    
    z_lower = min(cavity_dims[0,2],cavity_dims[1,2]) + (layer-1)*dz
    z_upper = max(cavity_dims[0,2],cavity_dims[1,2]) + (layer-1)*dz
    
    if (translate_z>0):
        z_lower += translate_z - np.floor((z_lower + translate_z)/L)*L
        z_upper += translate_z - np.floor((z_upper + translate_z)/L)*L
        allWaterPos[:,2] += translate_z - np.floor((allWaterPos[:,2] + translate_z)/L)*L
    # first retain only water molecules that are within cavity
    # I'm sure there is a more efficient way to do this
    # Add a 0.2 nm buffer on each side for x/y positions, but ignore for z I suppose. Could add Gaussian in this dimension, too

    maskx = (allWaterPos[:,0] >= cavity_dims[0,0]-buffer) & (allWaterPos[:,0] <= cavity_dims[1,0]+buffer)
    masky = (allWaterPos[:,1] >= cavity_dims[0,1]-buffer) & (allWaterPos[:,1] <= cavity_dims[1,1]+buffer)
    maskz = (allWaterPos[:,2] >= z_lower) & (allWaterPos[:,2] <= z_upper)
    waterInSlice = allWaterPos[maskx & masky & maskz]
    waterInSlice = waterInSlice[:,:2]
            
    # Iterate across all histograms and distribute water density using kernel function
    # Output has a series of rows in order from the top of the image to the bottom; so curhisto[0] is pixel at top left of image
    # and last value is bottom right. This matches format for .pgm images.
    cur_histo = np.zeros((numcolumns*numrows))
    for i in range(0, numrows):
        for j in range(0, numcolumns):
            # set spatial bounds on current histogram, which should match the image format.
            xmin = j*xhistowidth+cavity_dims[0,0] 
            xmax = (j+1)*xhistowidth+cavity_dims[0,0]
            # Note that i=0 corresponds to top left corner, so have to offset here.
            ymin = (numrows-i-1)*yhistowidth+cavity_dims[0, 1]
            ymax = (numrows-i)*yhistowidth+cavity_dims[0, 1]
            
            
            maskx_int = (waterInSlice[:,0] >= xmin - int_buffer) & (waterInSlice[:,0] <= xmax + int_buffer)
            masky_int = (waterInSlice[:,1] >= ymin - int_buffer) & (waterInSlice[:,1] <= ymax + int_buffer)
            
            int_waterInSlice = waterInSlice[maskx_int & masky_int]
            # now iterate over all water molecules that are in bounds of interest
            for pos in int_waterInSlice: 
                # pass position through kernel function to populate array
                # note that we can separately integrate x/y here
                xkern = integrate.quad(gaussian_kernel_function, xmin, xmax, args=(pos[0],halfxVec,))[0]
                ykern = integrate.quad(gaussian_kernel_function, ymin, ymax, args=(pos[1],halfyVec,))[0]
                cur_histo[i*numcolumns + j] += xkern*ykern
    return cur_histo

# %% Import libraries, files, and data to be used in script, then execute   
if __name__ == "__main__":    
    # Read input parameters from command line    

#    ligand = 'butanethiol'
#    z = 1.81 # Height of "surface" from bottom of box
#    dz = 0.3 # Width of surface testing
#    sidelength = 3.2 # Length along x and y directions
#    numrows = 32 # numx, numy - number of pixels in x and y direction
#    numcolumns = 32
#    input_top = 'sam_' + ligand + '_prod.gro' # input files
#    input_xtc = 'sam_' + ligand + '_prod_1.xtc'
#    output_prefix = 'trajAK'
#    layer = 1
#    translate_z = 0 # nm
#    O_charge = 0.84
#    H_charge = -0.42
    
    z = float(sys.argv[1]) # z value for center of slice
    dz = float(sys.argv[2]) # thickness of slice 
    sidelength = float(sys.argv[3]) # length of side for box
    numcolumns = int(sys.argv[4]) # number of bins in each dimension (columns = x, rows = y)
    numrows = int(sys.argv[5])
    input_top = sys.argv[6] # input files
    input_xtc = sys.argv[7] 
    output_prefix = sys.argv[8]
    layer = int(sys.argv[9])   # number of layer density to be calculated 
    translate_z = float(sys.argv[10]) # nm
    O_charge = float(sys.argv[11])
    H_charge = float(sys.argv[12])
    x_pos = float(sys.argv[13])
    y_pos = float(sys.argv[14])
    # variables needed for Gaussian kernel; width of smearing param in nm
    # 0.05 smearing function decays to 0 for +- 0.2 nm or so. 
    global smear, buffer, kernel_prefix, exponent_prefix
    smear = float(sys.argv[15])
    
    buffer = 0.2 # nm
    int_buffer = 0.2 # nm
    
    # Normalized so that integral from -inf to inf is 1. 0.39... = 1/sqrt(2pi)
    kernel_prefix = 0.3989422804*1.0/smear
    exponent_prefix = -0.5*(1.0/(smear*smear))
    
    start_time = time.time()
    print("Input parameters: z = ", z, "; Thickness = ", dz, "; Side length = ", sidelength,"; Histograms: rows ", numrows, " by columns ", numcolumns)
    print("Loading topology ",input_top, "; Loading trajectory ",input_xtc)
    traj = md.load(input_xtc, top=input_top,stride=1)
    nFrames = int(traj.time.size)
       
    # get solvent atom indices
    ndxWater_O = traj.topology.select( 'water and type O' ) # find heavy water atoms
    ndxWater_H = traj.topology.select( 'water and type H' ) # find water H atoms
    
    # set up histogram parameters here, assuming a NVT simulation
    # first row is min, second row is max
    cavity_dims = np.zeros((2, 3), dtype=np.float)
    
    # get box vectors - assumes they are constant
    boxVec = traj.unitcell_lengths[0, :]
    
    # get center of box
    if ((x_pos == -1)):
        halfxVec = 0.5*boxVec[0]
        halfyVec = 0.5*boxVec[1]
    else:
        halfxVec = x_pos
        halfyVec = y_pos
    
    # z-dimensional length
    L = boxVec[2]
    
    # calculate bounds in all three axes for cavity we care about
    cavity_dims[0, 0] = halfxVec - sidelength*0.5
    cavity_dims[1, 0] = halfxVec + sidelength*0.5
    cavity_dims[0, 1] = halfyVec - sidelength*0.5
    cavity_dims[1, 1] = halfyVec + sidelength*0.5
    cavity_dims[0, 2] = z - dz*0.5
    cavity_dims[1, 2] = z + dz*0.5
    
    # set up histograms here, including bounds (to avoid excess calculations)
    # Will reset as needed
    xhistowidth = sidelength / numcolumns
    yhistowidth = sidelength / numrows
    
    # initialize all histograms here - will save to disk at end
    all_histos = np.zeros((nFrames, numcolumns*numrows))
       
    # Iterate over all frames and populate array

    for i in range(0, nFrames):
        print("Calculating for frame ", i)
        temp_O = calc_solvent_density(traj, ndxWater_O, i, numcolumns, numrows, xhistowidth, yhistowidth, halfxVec, halfyVec, cavity_dims, layer, translate_z, L, boxVec)
        temp_H = calc_solvent_density(traj, ndxWater_H, i, numcolumns, numrows, xhistowidth, yhistowidth, halfxVec, halfyVec, cavity_dims, layer, translate_z, L, boxVec)
        all_histos[i, :] = temp_O*O_charge + temp_H*H_charge

    # save the raw histograms to disk
    output_file = open(output_prefix + '_' + str(layer) +  '_histos.pickle', "wb")
    # write pickle to disk, which can later be loaded for re-averaging, generating images, etc.
    pickle.dump(all_histos, output_file)
    output_file.close()
    end_time = np.round((time.time() - start_time)/60,1)
    print(  str(end_time) + 'mins')
