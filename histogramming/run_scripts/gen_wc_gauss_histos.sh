#!/bin/bash

#SBATCH -p compute
#SBATCH -t 24:00:00
#SBATCH -J CDsmear_all-histos
#SBATCH --nodes=1
#SBATCH --mail-user=kelkar2@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# input parameters

source ~/.bashrc
source ${master_loc}/ideal_ligand_master.sh
source ${master_loc}/system_specs.sh

base_dir=/home/kelkar2/CNN_Hydrophobicity/reference_sams

folder_pre=${system}_${numSAM}_${numLig_X}x${numLig_Y}_${T}K
folder_post=${water_model}_${ensemble}_${forcefield}

histogram_dir=$PWD/../all_histos
density_label=$1 # Input gauss or CDsmear or voxelCount

mixed_sam=no

for (( j=0; j<${#ligands[@]}; j++)); do
	
	ligand=${ligands[j]}
	echo $ligand
	z=${peak_positions[j]}
	dz=0.3
	side=2.0 # size of box in x-y plane, per dimension
	numx=20 # same dimensions as CIFAR-10
	numy=20
	layer=1 #controls which layer of water molecules you wish to histogram
	translate_z=0 # nm # move the box by this distance along the z co-ordinate
	x_pos=-1
	y_pos=-1	

	O_charge=-0.8476
	H_charge=0.4238

	#O_charge=1.00
	#H_charge=0.00
	
	input_dir=${base_dir}/${folder_pre}_wc_${ligand}_${folder_post}/
	top=${input_dir}sam_prod.gro
        inputprefix=${input_dir}/split_traj/sam_${ligand}

	mkdir ${histogram_dir}/${density_label}
	outputfolder=${histogram_dir}/${density_label}/sam_wc_${ligand}_${density_label} # CD - charge density
	#outputfolder=bulk_water_smear05
	outputprefix=${outputfolder}/traj

	if [[ $mixed_sam == "yes" ]]
	then
		grid_search_output=${input_dir}/output_files
		mkdir -p $grid_search_output
		ideal_fraction="$(echo ${ligand} | cut -d"_" -f2)" 
		box_dims=$( tail -n 1 $top )
		n=10
		cavity_dims=( ${side} ${side} ${dz} )
		echo $cavity_dims
		in_file=sam_prod
		/usr/bin/python3.4 -i ../python_scripts/place_probe_grid.py ${input_dir} ${box_dims} ${n} ${ideal_fraction} ${z} ${in_file} ${side} ${side} ${dz}	
		cavity_position=$(tail -n 1 ${grid_search_output}/new_sam_slab_coordinates.csv)
		cavity_position=${cavity_position##*: }
		x_pos="$(echo ${cavity_position} | cut -d"," -f2)"
		y_pos="$(echo ${cavity_position} | cut -d"," -f3)"
	fi

	# number of trajectories to run on, one python instance per
	num_trajs=25
	
	mkdir -p ${outputfolder}
	
	for (( i=0; i<${num_trajs}; i++)); do
	   # Spawn python instance
	   cur_traj=${inputprefix}_${i}.xtc
	   cur_output=${outputprefix}_${i}
	   echo "Running for traj " 
	   echo $cur_traj
	   # puts in background to run concurrently
	   /usr/bin/python3.4 ../python_scripts/gen_${density_label}_histos.py ${z} ${dz} ${side} ${numx} ${numy} ${top} ${cur_traj} ${cur_output} ${layer} ${translate_z} ${O_charge} ${H_charge} ${x_pos} ${y_pos}&
	   # sleep 
	done
	
	# deactivate
	
	# make sure to wait for jobs to finish
	wait

done

wait
