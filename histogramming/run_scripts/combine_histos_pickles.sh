#!/bin/bash

source ~/.bashrc
source ${master_loc}/wc_ideal_all_0.2ps_ligand_master.sh 
#ligands=('wc_hydroxy-undecanethiol_k1.0')

smear=0.05
#size=2.0x2.0x0.3nm_20x20_smear${smear} # Leave blank if 3.2x3.2x0.3nm
size=2.0x2.0x0.3nm_20x20
if [ $size != 3.2x3.2x0.3nm ]
then
	ligands=( "${ligands[@]/#/${size}_}" )
fi

echo ${ligands[@]}

density_rep=$1 # gauss or CDsmear or voxelCount
input_folder=${CNN_home}/histogramming/all_histos/${density_rep}
lig_input_prefix=sam_ 
lig_input_postfix=_${density_rep}/
pickle_prefix=traj_
pickle_postfix=_1_histos.pickle
num_trajs=25
num="$(echo $size | cut -d"_" -f2)"
numx="$(echo $num | cut -d"x" -f1)"
numy="$(echo $num | cut -d"x" -f2)"
n=${#ligands[@]}

/usr/bin/python3.4 ../python_scripts/combine_histos_pickles.py ${input_folder}/ ${lig_input_prefix} ${lig_input_postfix} ${pickle_prefix} ${pickle_postfix} ${num_trajs} ${numx} ${numy} ${density_rep}_ ${n} ${ligands[@]}


