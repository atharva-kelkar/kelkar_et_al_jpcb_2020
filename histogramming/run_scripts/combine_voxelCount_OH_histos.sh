#!/bin/bash

source ~/.bashrc
source ${master_loc}/wc_mfstudy_all_ligand_master.sh 
#ligands=('wc_tridecanethiol_long')

smear=0.05
#size=2.0x2.0x0.3nm_20x20_smear${smear} # Leave blank if 3.2x3.2x0.3nm
size=2.0x2.0x0.3nm_20x20
if [ $size != 3.2x3.2x0.3nm ]
then
	ligands=( "${ligands[@]/#/${size}_}" )
fi

echo ${ligands[@]}

density_rep=$1 # gauss or CDsmear or voxelCount
size_folder=$2 # 6x6 or 8x8 or 12x12
input_folder=${CNN_home}/histogramming/all_histos/${size_folder}/${density_rep}
lig_input_prefix=sam_ 
lig_input_postfix=_${density_rep}/
pickle_prefix=traj_
pickle_postfix=_1_histos.pickle
num_trajs=25
num="$(echo $size | cut -d"_" -f2)"
numx="$(echo $num | cut -d"x" -f1)"
numy="$(echo $num | cut -d"x" -f2)"
n=${#ligands[@]}

/usr/bin/python3.4 ../python_scripts/combine_voxelCount_OH_histos_pickles.py ${input_folder}/ ${lig_input_prefix} ${lig_input_postfix} ${pickle_prefix} ${pickle_postfix} ${num_trajs} ${numx} ${numy} ${density_rep}_ ${n} ${ligands[@]}


