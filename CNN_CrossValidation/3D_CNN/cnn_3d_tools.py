# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 3/16/2020

"""
import numpy as np
import pickle 

##############################################################
### Load arrays given folder and file name
##############################################################

def load_array( input_folder, file_name ):
    pickle_in = open( input_folder + file_name, "rb" )
    arr = pickle.load(pickle_in)
    pickle_in.close()
    return np.array( arr )

##############################################################
### Define file names of arrays
##############################################################

def define_file_names( ):
    train_label = 'ideal'
    test_label = 'ising'
    data_rep = 'voxelCount_t10f'
    size = '2.0x2.0x0.3nm_20x20'
    size_test = '2.0x2.0x0.3nm_'
    CNStat = 'All'
    
    train_input_folder = '/home/kelkar2/CNN_Hydrophobicity/CNN_Input/Input/CNN_Input_' + train_label + '_' + data_rep \
                            + '_' + size + '_' + CNStat + '/'
    val_input_folder = train_input_folder
    test_input_folder = '/home/kelkar2/CNN_Hydrophobicity/CNN_Input/Input/CNN_Input_' + test_label + '_' + data_rep \
                            + '_' + size_test + '_' + CNStat + '/'    
    return train_input_folder, val_input_folder, test_input_folder

##############################################################
### Normalize arrays with overall max or frame-wise max values
##############################################################
    
def normalize_X_arrays( X, nDeep, nChannels, norm_method = 'frame' ):
    if (norm_method == 'all'):
        X /= np.max(X)
        print('Arrays normalized with max value')

    if (norm_method == 'frame'):
        if (len(X.shape) == 4):
            nDeep = X.shape[-1]
            train_max_1 = np.zeros((X.shape[0], 1))
            for count_1 in range(nDeep):
                train_max_0 = np.array([np.max(x) for x in X[ :, :, :, count_1]])
                train_max_0 = np.reshape(train_max_0, (train_max_0.shape[0],1))
                train_max_1 = np.hstack((train_max_1, train_max_0))
            train_max_1 = np.delete(train_max_1, 0, axis=1)
            
            X /= train_max_1[ : , np.newaxis , np.newaxis , : ]
            print('Arrays normalized with frame-wise max value')
        elif (len(X.shape) == 5):
            for x in X:
                for count_1 in range(X.shape[-2]):
                    for count_2 in range(X.shape[-1]):
                        x[ :, :, count_1, count_2 ] = x[ :, :, count_1, count_2 ]/np.max( x[ :, :, count_1, count_2 ] )
            
    return X

def normalize_y_arrays( y ):
    return (y - np.mean( y ))/np.std( y )

##############################################################
### Change shape from 4D to 5D to suit 3D CNN input
##############################################################
    
def arr_4d_to_5d( X ):
    return X.reshape( X.shape[0], X.shape[1], X.shape[2], X.shape[3], 1)

##############################################################
### Function that removes or chooses elements from given X and y arrays
##############################################################

def choose_subset( X, y, choose_flag, i, j, k, nDeep ):
    y_unique = np.unique( y )
    if (choose_flag == "choose"):
        mask = np.all((y != y_unique[i], y != y_unique[j], y != y_unique[k]), axis=0)
    if (choose_flag == "delete"):
        mask = np.any((y == y_unique[i], y == y_unique[j], y == y_unique[k]), axis=0)
    y = np.delete(y, np.where(mask), axis=0)
    X = np.delete(X, np.where(mask), axis=0)
    if (nDeep==1):
        X = X[:,:,:,0]
    shape = np.shape(X)
    X = X.reshape((shape[0], shape[1], shape[2], nDeep)) # Change 4th dimension to 3 for RGB images and 1 for single colour image
    return X, y
