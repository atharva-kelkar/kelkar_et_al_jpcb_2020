# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 3/16/2020

Code: 3-D CNN using the Functional API in Keras
"""

import pickle
import numpy as np
from keras.utils import plot_model
from keras.models import Model
from keras.layers import Input
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv3D
from keras.layers.pooling import MaxPooling3D
import tensorflow.keras.backend as K
import keras
from cnn_3d_tools import define_file_names, normalize_X_arrays, normalize_y_arrays, arr_4d_to_5d, choose_subset
import sys

def load_array( input_folder, file_name ):
    pickle_in = open( input_folder + file_name, "rb" )
    arr = pickle.load(pickle_in)
    pickle_in.close()
    return np.array( arr )

def load_and_concatenate( curr, input_folder, arr_str ):
    temp = load_array( input_folder, arr_str )
    return np.concatenate(( curr , temp ))

def choose_validation_set(file_name, index):
	pickle_in = open(file_name, 'rb')
	nfold_arr = pickle.load(pickle_in)
	pickle_in.close()
	validation_set = nfold_arr[ index ]
	return validation_set

if __name__ == "__main__":

    
    ##############################################################
    ### Defining pickle file names, depth of network, output folder
    ##############################################################
#    train_input_folder, validate_input_folder, test_input_folder = define_file_names()
    train_input_folder = sys.argv[1]
    validate_input_folder = train_input_folder
#    validate_input_folder = sys.argv[2]
#    test_input_folder = sys.argv[3]
    nDeep = int(sys.argv[2])
    index = 4 + int(sys.argv[3])
    i, j, k = index, index, index
    output_folder = ""
    norm_method = 'frame'
    img_rows, img_cols = int(sys.argv[4]), int(sys.argv[5])
    n_split = int(sys.argv[6])
    file_name = sys.argv[7]
    nChannels = int(sys.argv[8])
        
    ##############################################################
    ### Global variable definitions 
    ##############################################################
    K.clear_session()
    batch_size = 256 
    nb_epoch = 500
    validation_split = 0.1
    optimizer = keras.optimizers.Adamax(0.00002)
    in_shape = (img_rows, img_cols, nDeep, nChannels)
    print(in_shape)
    ##############################################################
    ### Loading pickles for training and validation data
    ##############################################################
    
    if (n_split == -1):
        X_train_curr = load_array( train_input_folder, 'X_train_np.pickle' )
        y_train_curr = load_array( train_input_folder, 'y_train.pickle' )
        X_validate_curr = load_array( validate_input_folder, 'X_validate_np.pickle' )
        y_validate_curr = load_array( validate_input_folder, 'y_validate.pickle' )
    elif (n_split > 0):
        for i in range(n_split):
            train_x_str = 'X_train_np_' + str(i) + '.pickle'
            val_x_str = 'X_validate_np_' + str(i) + '.pickle'
            train_y_str = 'y_train_' + str(i) + '.pickle'
            val_y_str = 'y_validate_' + str(i) + '.pickle'
            if (i == 0):
                X_train_curr = load_array( train_input_folder, train_x_str )
                X_validate_curr = load_array( validate_input_folder, val_x_str )
                y_train_curr = load_array( train_input_folder, train_y_str )
                y_validate_curr = load_array( validate_input_folder, val_y_str )
            else:
                X_train_curr = load_and_concatenate( X_train_curr, train_input_folder, train_x_str )
                X_validate_curr = load_and_concatenate( X_validate_curr, validate_input_folder, val_x_str )
                y_train_curr = load_and_concatenate( y_train_curr, train_input_folder, train_y_str )
                y_validate_curr = load_and_concatenate( y_validate_curr, validate_input_folder, val_y_str )
    
    
    
    ##############################################################
    ### Adjust datatype of arrays
    ##############################################################
    X_train_curr = X_train_curr.astype('float16')
    X_validate_curr = X_validate_curr.astype('float16')

    print('Converted X arrays to float16')
    y_train_curr = y_train_curr.astype('float32')
    y_validate_curr = y_validate_curr.astype('float32')
    #np.random.shuffle( y_validate_curr )
    ##############################################################
    ### Normalize X arrays
    ##############################################################
    X_train_curr = normalize_X_arrays( X_train_curr, nDeep, nChannels, norm_method )
    X_validate_curr = normalize_X_arrays( X_validate_curr, nDeep, nChannels, norm_method )
    
    ##############################################################
    ### Filter out relevant portions of arrays for n-fold CV
    ##############################################################
    y_unique = np.unique( y_train_curr )
    
    validation_entries_ind = choose_validation_set( file_name, index )
    validate_entries = y_unique[ validation_entries_ind ]
    
    for val in validate_entries:
        X_train_curr = np.delete(X_train_curr, np.where( y_train_curr == val), axis = 0)
        y_train_curr = np.delete(y_train_curr, np.where( y_train_curr == val), axis = 0)

    ##############################################################
    ### Filter out relevant portions of arrays for a leave-1-out CV process
    ##############################################################
    X_train_curr, y_train_curr = choose_subset( X_train_curr, y_train_curr, "delete", i, j, k, nDeep )
    X_validate_curr, y_validate_curr = choose_subset( X_validate_curr, y_validate_curr, "choose", i, j, k, nDeep )

    ##############################################################
    ### Reshape X arrays to the right shape, normalize y arrays
    ##############################################################
    if (len(X_train_curr.shape) == 4):
        X_train_curr = arr_4d_to_5d( X_train_curr )
        X_validate_curr = arr_4d_to_5d( X_validate_curr )
    y_train_norm = normalize_y_arrays( y_train_curr )
    y_validate_norm = normalize_y_arrays( y_validate_curr )
    print('X_train shape:', X_train_curr.shape)
    print('Y_train shape:', len(y_train_curr))
    print('X_test shape:', X_validate_curr.shape)
    
    ##############################################################
    ### Defining model layers
    ##############################################################
    visible = Input( shape = in_shape )
    conv1 = Conv3D( 25, kernel_size = 3 , \
                   strides = [1, 1, 1] ,\
                   activation = 'relu' ,\
                   )(visible)
        
    maxPool1 = MaxPooling3D( pool_size = (2, 2, 1) )(conv1)
    conv2 = Conv3D( 12, kernel_size = [3, 3, 3] ,\
                   strides = [1, 1, 1] ,\
                   activation = 'relu' ,\
                   )(maxPool1)
        
    maxPool2 = MaxPooling3D( pool_size = (2, 2, 1) )(conv2)
    flat = Flatten()(maxPool2)
    dense1 = Dense( 180, activation = 'relu' )(flat)
    dropout1 = Dropout(0.5)(dense1)
    dense2 = Dense( 50, activation = 'relu' )(dropout1)
    dropout2 = Dropout(0.5)(dense2)
    output = Dense( 1, activation = 'linear' )(dropout2)
    model = Model(inputs=visible , outputs=output)
    
    ##############################################################
    ### Compile model with optimizer, loss, error metrics
    ##############################################################
    model.compile(optimizer, \
                  loss='mse', \
                  metrics = ['mse'] )
            
    history = model.fit(X_train_curr, y_train_norm, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, validation_split=0.2)
        
    print( model.summary() )
    
    ##############################################################
    ### Save model
    ##############################################################
    model.save( '3d_cnn_voxelCount_t' + str(nDeep) + '_' + str(index) + 'f.h5' )

    ##############################################################
    ### Predict for validation set
    ##############################################################
    y_pred = model.predict( X_validate_curr )
    y_pred = y_pred * np.std( y_train_curr ) + np.mean( y_train_curr ) 
    
    ##############################################################
    ### Save model parameters
    ##############################################################
    dict = {'history': history.history,
            'mean': np.mean( y_train_curr ),
            'std': np.std( y_train_curr ),
            'y_unique': y_unique,
            'y_pred': y_pred,
            'y_train_unique': np.unique( y_train_curr ),
            'y_val_unique': np.unique( y_validate_curr ),
            'X_val': X_validate_curr,
            'y_val': y_validate_curr,
            'validate_entries': validate_entries
            }
    file_name = output_folder + 'regress_store_' + str(i) + '_j_' + str(j) + '_k_' + str(k) + '.pickle'
    pickle_out = open(file_name, 'wb')
    pickle.dump(dict, pickle_out)
    pickle_out.close()

