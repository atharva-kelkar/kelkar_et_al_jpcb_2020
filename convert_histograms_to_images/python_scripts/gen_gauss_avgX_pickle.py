# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 13:48:47 2019

@author: kelkar2
"""

import numpy as np
import pickle
import sys

input_file = sys.argv[1]
ligand = sys.argv[2]
nAvg = int(sys.argv[3])
output_file = sys.argv[4]

#input_file = '/home/kelkar2/for_atharva/histos/gauss_pickles_All/gauss_dodecanethiol.pickle'
#ligand = 'dodecanethiol'
#nStack = 3
#output_file = ""

images = np.load(input_file)

shape = np.shape(images)

#all_img = np.zeros((1, shape[1], shape[2], nStack))

#images = images[1:shape[0] + 1 - int(np.mod(shape[0],nStack)),:,:]

#shape = np.shape(images)

#imges = images.reshape((shape[0], shape[1], shape[2], 1))

all_img = np.zeros((shape[0] - nAvg, shape[1], shape[2], 1))

for i in range( 0, shape[0] - nAvg ):
    
    print(i)
    temp = np.mean(images[i:i+nAvg,:,:], axis=0).reshape((shape[1], shape[2], 1))
    
    all_img[i,:,:,:] = temp
    
dict = {'images': all_img,
        'ligand': ligand}

pickle_out = open(output_file, 'wb')
pickle.dump(dict, pickle_out)
pickle_out.close()
