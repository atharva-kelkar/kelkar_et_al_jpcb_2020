# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 13:48:47 2019

@author: kelkar2
"""

import numpy as np
import pickle
import sys

input_file = sys.argv[1]
ligand = sys.argv[2]
nStack = int(sys.argv[3])
output_file = sys.argv[4]
nChannels = 2 # For
#input_file = '/home/kelkar2/for_atharva/histos/gauss_pickles_All/gauss_dodecanethiol.pickle'
#ligand = 'dodecanethiol'
#nStack = 3
#output_file = ""

images = np.load(input_file)

shape = np.shape(images)

images = images[1:shape[0] + 1 - int(np.mod(shape[0],nStack)), : ]

shape = np.shape(images)

all_img = np.zeros((int(shape[0]/nStack), shape[2], shape[3], nStack, nChannels))

for i in range(0, int(shape[0]/nStack)):
    print(i)
    for j in range(nStack):
        temp_1 = images[i*nStack + j]
#        print('temp_1 shape:', temp_1.shape)
        temp_1 = np.stack( (temp_1[0][:,:,np.newaxis], temp_1[1][:,:,np.newaxis]) , axis = -1)
#        print('temp_1 modified shape:', temp_1.shape)
        if j == 0:
            temp = np.copy(temp_1)
        else:
            temp = np.concatenate( (temp, temp_1) , axis = 2 )
#    print(temp.shape)
    all_img[i,:,:,:,:] = temp
    
dict = {'images': all_img,
        'ligand': ligand}

pickle_out = open(output_file, 'wb')
pickle.dump(dict, pickle_out)
pickle_out.close()