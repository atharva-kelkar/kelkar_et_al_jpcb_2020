#!/bin/bash

# generates input for deep learning algo in CIFAR format from input numpy histograms
# containing density information. Allows us to tune how many frames we average over, 
# for example.

# input parameters

# time_frame=3 # how many frames to average over
# maxframes=1600 # maximum number of frames/pickle
# num_trajs=25 # how many pickles to read in

list_name=$3
source ~/.bashrc
source ${master_loc}/${list_name}

density_rep=$1 # Or gauss or CDsmear or voxelCount
nStack=$2
label=${density_rep}_t${nStack}f
size=$4
size_folder=$5
numReplicate=$6

if [ $size != "3.2x3.2x0.3nm" ]
then
        ligands=( "${ligands[@]/#/${size}_}" )
fi

input_dir=${CNN_home}/histogramming/all_histos/${size_folder}/${density_rep}/replicate${numReplicate}

output_dir=${CNN_home}/images/all_image_types/${size_folder}/${label}/replicate${numReplicate}

mkdir -p ${output_dir}/

count=0

for (( i=0; i<${#ligands[@]}; i++)); do
   ligand=${ligands[i]}

   # iterate over each input pickle for interface/bulk respectively
   input_file=${input_dir}/${density_rep}_${ligands[i]}.pickle
   output_file=${output_dir}/${density_rep}_t${nStack}f_${ligand}.pickle
   echo $cur_input
   /usr/bin/python3.4 ../python_scripts/gen_gauss_tXf_pickle.py ${input_file} ${ligand} ${nStack} ${output_file}
   count=$(( count+1 ))
done

wait

