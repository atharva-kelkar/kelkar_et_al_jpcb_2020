#!/bin/bash

density_reps=('voxelCount_OH')
num_arr=(1 2 3 4 6 10 15 20 25 30)
list_name=wc_mfstudy_all_ligand_master.sh
dataset=patterned # ideal or ising or patterned
smear=0.05
size=2.0x2.0x0.3nm_20x20 #_smear${smear}
size_folder="/" # 
numReplicate=-1
base_dir=$PWD

if [[ $dataset == 'ising' ]]; then
	base_dir=$PWD/$dataset
fi

echo ${size}

for density_rep in ${density_reps[@]}; do
	for (( i=0; i<${#num_arr[@]}; i++ )); do

        	echo ${num_arr[i]}
		#bash gen_avgX_images.sh $density_rep ${num_arr[i]} ${list_name} ${size}
		#bash ${base_dir}/gen_txf_images.sh $density_rep ${num_arr[i]} ${list_name} ${size} ${size_folder} ${numReplicate}
		#bash gen_AvgVar_images.sh $density_rep ${a[i]}
		#bash gen_txf_overlap_images.sh ${density_rep} ${a[i]} ${list_name} ${size}
		bash ${base_dir}/gen_txf_OH_images.sh $density_rep ${num_arr[i]} ${list_name} ${size} ${size_folder} ${numReplicate}
		#bash gen_txf_OHS_images.sh $density_rep ${num_arr[i]} ${list_name} ${size} ${size_folder} ${numReplicate}
	done
done



