#!/bin/bash

# generates input for deep learning algo in CIFAR format from input numpy histograms
# containing density information. Allows us to tune how many frames we average over, 
# for example.

# input parameters

# time_frame=3 # how many frames to average over
# maxframes=1600 # maximum number of frames/pickle
# num_trajs=25 # how many pickles to read in

list_name=$3
source ~/.bashrc
source ${master_loc}/${list_name}

numx=20 # dimensions of input histograms
numy=20
dz=0.3
sidelength=2.0
density_rep=$1 # CDsmear or gauss or AvgVar
data_rep=$1
nAvg=$2
label=${data_rep}_avg${nAvg}
size=$4

if [ $size != "3.2x3.2x0.3nm" ]
then
        ligands=( "${ligands[@]/#/${size}_}" )
fi

input_dir=${CNN_home}/histogramming/all_histos/${density_rep}

output_dir=${CNN_home}/images/all_image_types/${label}

mkdir -p ${output_dir}/

count=0

for (( i=0; i<${#ligands[@]}; i++)); do
   ligand=${ligands[i]}

   # iterate over each input pickle for interface/bulk respectively
   input_file=${input_dir}/${density_rep}_${ligands[i]}.pickle
   output_file=${output_dir}/${label}_${ligand}.pickle
   echo $cur_input
   /usr/bin/python3.4 ../python_scripts/gen_gauss_avgX_pickle.py ${input_file} ${ligand} ${nAvg} ${output_file}
   count=$(( count+1 ))
done

wait

