# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 15 May 2020
"""
'''
Run script as

    python3.4 gen_min_simulation_time.py -f ${folder_name} -i ${file_name} -n ${numFiles}

Inputs
    folder_name:
        Name of folder that contains results files
    file_name:
        Name of pickle output files (with INDEX in the place of pickle index)
    n:
        Number of folds validated over (Default: 5-fold CV, n = 5)
'''

import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser # Used to allow commands within command line

def calc_val(data, pred_label, indus_label, x, test_ps, n_samples):
    for n in range(n_samples):
        temp_val = np.mean(data[pred_label][ data[indus_label] == x ][n*test_ps:(n+1)*test_ps])
        if (n == 0):
            val = temp_val
        else:
            val = val + temp_val
    return (val/n_samples)

if __name__ == "__main__":

    def_folder = '/home/kelkar2/CNN_Hydrophobicity/CNN_Output/CHTC_Output/3D_CNN_dropout_n-fold/RegressionOutput_ideal_all_voxelCount_OH_t30f_2.0x2.0x0.3nm_20x20_All/'
    def_file = 'regress_store_INDEX_j_INDEX_k_INDEX.pickle'
        
    testing = False
    if testing:
        args = { 'folder_name': def_folder,
                 'file_name': def_file,
                 'n': 5,
                }   
    else:

        use = 'Usage: %prog [options]'
        parser = OptionParser(usage = use)
        parser.add_option( '-f', '--folder_name', dest = 'folder_name', action = 'store', type = 'string', help = 'folder of results files', default = def_folder )
        parser.add_option( '-i', '--file_name', dest = 'file_name', action = 'store', type = 'string', help = 'file name of storage files', default = def_file )
        parser.add_option( '-n', '--numFiles', dest = 'n', action = 'store', type = 'int', help = 'Number of results files', default = 5 )
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        args = { 'folder_name': options.folder_name,
                 'file_name': options.file_name,
                 'n': options.n
                 }
    
    nStack = 30
    test_ps_arr = np.arange(30, 1000, 30)
    test_ps_arr = (test_ps_arr/nStack).astype('int32')
    res = np.zeros( (1, 3) )
    
    for i in range(args['n']):
        print(i)
        temp_file_name = args['file_name'].replace('INDEX', str(i))
        temp_file = args['folder_name'] + temp_file_name
        data = np.load( temp_file )
        for test_ps in test_ps_arr:
            print(test_ps)
            pred_all_temp = [(test_ps, x, calc_val(data, 'y_pred', 'y_val', x, test_ps, 1) ) \
                            for x in np.unique(data['validate_entries'])]
            pred_all_temp = np.array(pred_all_temp)
            res = np.concatenate((res, pred_all_temp))
#    np.delete(res, 0)        
    np.savetxt( "sim_time.csv", res, delimiter = ",", header = "Time (ps),INDUS value (kT),CNN Pred (kT)")
#    with open('sim_time.csv', 'wb') as f:
#        f.write()
                
            
            
    

            
            
            
            
            
            
            
            
            
        
        
        