# -*- coding: utf-8 -*-
"""
@author: Atharva Kelkar
@date: 15 May 2020
"""
'''
Run script as

    python3.4 gen_min_simulation_time.py sim_time.csv

Inputs:
    sim_time.csv
        File with headers - 
            # Time (ps), INDUS value (kT), CNN Pred (kT)

'''

import numpy as np
import pandas as pd
import sys
import matplotlib.pyplot as plt
plt.rcParams

def calc_rmse(x):
    return np.sqrt(np.sum( (x[:,1] - x[:,2])**2 )/ x.shape[0])

def make_column(x):
    return x.reshape( x.shape[0] , 1 )
    
def stack(x, y, z):
    return np.hstack( (make_column(x), make_column(y), make_column(z)) )

def plot_graph(x, y):
    plt.rcParams["figure.figsize"] = (4,4)
    fontsize = 10
    ticksize = 10
    markersize = 8
    fig, ax = plt.subplots()
    ax.plot( x, y, 'o', markersize = markersize)
    ax.set_xlabel('MD simulation time averaged over for prediction (ps)', fontsize = fontsize)
    ax.set_ylabel('RMSE (kT)', fontsize = fontsize)
    ax.tick_params(axis='both', which='major', labelsize = ticksize)
    for axis in ['top','bottom','left','right']:
      ax.spines[axis].set_linewidth(2)
    ax.grid( linestyle = "--" )
    return fig, ax

if __name__ == "__main__":
    
    testing = True
    if testing:
        file_name = r'sim_time.csv'
    else:
        file_name = sys.argv[1]
    nStack = 30
    data = pd.read_csv( file_name )
    time_arr = np.array( data["# Time (ps)"] )
    indus_arr = np.array( data["INDUS value (kT)"])
    cnn_arr = np.array( data['CNN Pred (kT)'] )
    comp_arr = stack( time_arr, indus_arr, cnn_arr )
    
    res = [ (int(x*30), calc_rmse( comp_arr[ comp_arr[:, 0] == x] )) for x in np.unique( comp_arr[:,0]) ]
    res = np.array( res )
    res = np.delete( res, 0, axis = 0 )
    
    fig, ax = plot_graph( res[:,0], res[:,1] )
    fig.savefig('20200518_min_simulation_time.svg', extension = 'svg', bbox_inches = 'tight', pad_inches = 0)