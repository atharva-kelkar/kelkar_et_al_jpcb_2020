# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 15:25:44 2019

@author: kelkar2
"""

import numpy as np
import Ising_MC_functions_new as imf
import matplotlib.pyplot as plt
import sys
import pickle

def Total_E(tot_num_lattice, pos, neighbours, spins, J, mu, H):
    energy_T = 0
    for i in range(tot_num_lattice):
            for j in range(len(neighbours[i])):
                energy_T+=-0.5*J*spins[i]*spins[neighbours[i][j]]
    energy_T+= -mu*H*np.sum(spins)
    return energy_T

def Calculate_dE(spin_rotate, pos, spins, neighbours, J, mu, H):
    E_old = 0
    E_new = 0
    for j in range(len(neighbours[spin_rotate])):
        E_old+= -J*spins[spin_rotate]*spins[neighbours[spin_rotate][j]]
    for j in range(len(neighbours[spin_rotate])):
        E_new+= -J*-spins[spin_rotate]*spins[neighbours[spin_rotate][j]]
    dE = E_new-E_old
    return dE

def choose_opposite_spin(spin_rotate, spins, tot_num_lattice):
    spin_curr = spins[spin_rotate]
    spin_second = spin_curr
    if np.max(spins) == np.min(spins):
#        print("condition hit")
        spin_rotate_second = -1
    else:
        while (spin_second == spin_curr):
            spin_rotate_second = np.random.randint(0, tot_num_lattice)
            spin_second = spins[spin_rotate_second]
#            print(str(spin_second))
    return spin_rotate_second


if __name__ == "__main__":

    testing = False
    if testing == False:
        mol_frac = float(sys.argv[1])    
        J = float(sys.argv[2])
        n = int(sys.argv[3])
        index = sys.argv[4] 
        output_folder = sys.argv[5]   
        n_tot = int(sys.argv[6])
    if testing == True:
        mol_frac = 0.5
        J = 10
        n = 6
        index = "-1"
        output_folder = "./"
        n_tot = 8
 
    mu = 1
    H = 1
    n_MC = 30000
    sample_time = 5
    energies = np.zeros(int(round(n_MC/sample_time,0)))
    magnetization = np.zeros(int(round(n_MC/sample_time,0)))
    

    pos, neighbours = imf.AssembleHexLattice_noPBC(n,n)
    tot_num_lattice, spins = imf.InitializeLattice(n, n, mol_frac)
    energy = Total_E(tot_num_lattice, pos, neighbours, spins, J, mu, H)
    
    spins_init = np.copy(np.reshape(spins, (n, n)))
#    lattice_init = np.zeros((n,n*2))
#    lattice_init[::2, 1::2] = spins_init[::2, :]
#    lattice_init[1::2, ::2] = spins_init[1::2, :]
#    plt.imshow(lattice_init)
#    plt.axis('off')
#    plt.show()
#    print(spins)
    nbd_cnt = 0
    for count in range(n_MC):
            spin_rotate_1 = np.random.randint(0, tot_num_lattice) # Choose any spin to rotate
            
            spin_rotate_2 = choose_opposite_spin(spin_rotate_1, spins, tot_num_lattice) # Choose 
            
#            print(spin_rotate_1, spin_rotate_2)
            
            if spin_rotate_2 == -1 :
                avcdef = 1
#                print("Could not find spin to rotate")
            else :
                dE_s1 = Calculate_dE(spin_rotate_1, pos, spins, neighbours, J, mu, H) # Calculate delta change in energy on rotating selected spin
                dE_s2 = Calculate_dE(spin_rotate_2, pos, spins, neighbours, J, mu, H) # Calculate delta change in energy on rotating second spin
                dE = dE_s1 + dE_s2
                if (spin_rotate_1 in neighbours[spin_rotate_2]):
                    nbd_cnt += 1
#                    print("neighbours chosen", str(nbd_cnt))
                    dE = dE - J*4
#                    print("neighbours chosen")
#                else:
#                    print("neighbours not chosen")
                alpha = min(np.exp(-dE),1)
                rand1 = np.random.uniform(0,1)
                if (rand1<alpha):
                    spins[spin_rotate_1] = spins[spin_rotate_1]*-1
                    spins[spin_rotate_2] = spins[spin_rotate_2]*-1
            
                if (np.mod(count,sample_time)==0): # Pointless if-else condition that exists due to legacy issues in the code
                    ind = int(count/sample_time)
                    if (ind>0):
                        energies[ind] = Total_E(tot_num_lattice, pos, neighbours, spins, J, mu, H)
                        magnetization[ind] = np.sum(spins)
                    else:
                        energies[ind] = Total_E(tot_num_lattice, pos, neighbours, spins, J, mu, H)
                        magnetization[ind] = np.sum(spins)
#    print(spins)
    spins_fin = spins.reshape((n,n))
#    lattice_fin = np.zeros((n,n*2))
#    lattice_fin[::2, 1::2] = spins_fin[::2, :]
#    lattice_fin[1::2, ::2] = spins_fin[1::2, :]
#    plt.imshow(lattice_fin)
#    plt.axis('off')
#    plt.show()
#    plt.plot(energies)
    
    spins_fin[spins_fin == -1] = 0
    spin_matrix = np.zeros(( n_tot, n_tot ))
    spin_matrix[:] = 2
    spin_matrix[:n, :n] = spins_fin
    if (index != "-1"):
        np.savetxt(output_folder + 'J_' + str(J) + '_molFrac_' + str(mol_frac) + '_' + index + '.txt', spins_fin.astype('int'), fmt = '%1d')
    else:
        np.savetxt(output_folder + 'J_' + str(J) + '_molFrac_' + str(mol_frac) + '_' + str(n) + 'x' + str(n) + '.dat', spin_matrix.astype('int'), fmt = '%1d')


