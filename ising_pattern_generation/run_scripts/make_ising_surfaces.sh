#!/bin/bash

n=5
#J_arr=(-1.0 -0.67 -0.33 0.0 0.33 0.67 1.0)
#mol_frac_arr=(0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)

J_arr=(-3.0 -1.0 -0.5 0.0 0.5 1.0 3.0)

mol_frac_arr=(0.25 0.50 0.75)


output_folder=../pattern_files_new/

mkdir -p ${output_folder}

for j in ${J_arr[@]}; do
	for mf in ${mol_frac_arr[@]}; do
		#for index in $(seq 0 1 10); do
			index=-1
			/usr/bin/python3.4 ../python_scripts/generateIsingSurfaces.py $mf $j $n $index $output_folder

		#done
	done
done



