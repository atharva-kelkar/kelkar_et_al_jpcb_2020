## Analysis Scripts for Kelkar et al. JPCB (2020)
**Authors**: Atharva S. Kelkar, Bradley C. Dallin, Reid C. Van Lehn

**DOI**: https://pubs.acs.org/doi/pdf/10.1021/acs.jpcb.0c05977

The directory is divided into sub-folders as follows:

a. **histogramming** – Scripts for converting MD trajectories to frame-wise input representations (density matrices) for the CNN, and combining parallelized histogram generating scripts 

b. **convert_histograms_to_images** – Scripts for converting frame-wise histograms to stacked or averaged representations for 2D or 3D CNNs

c. **CNN_CrossValidation** – Scripts for building and running 2D and 3D CNNs (Note that the 3D CNNs work with TensorFlow GPU whereas 2D CNNs work with a CPU implementation of TensorFlow)

d. **ising_pattern_generation** – Scripts for generating Ising patterns given pattern shape and Ising parameter J

e. **result_analysis** - Scripts for analyzing results from the CNN. Current version has benchmarking for simulation time needed for robust predictions

Each folder is divided into 2 subfolders - *run_scripts* and *python_scripts*. The *run_scripts* folder consists of bash scripts used to run Python scripts with relevant inputs. The *python_scripts* folder consists of Python scripts used to analyze data

## Notes about code nomenclature

The nomenclature used in the codes is slightly different than the one used in the paper. The correspondence is given below:

- Count Histograms = voxelCount
- Stack of *n* frames = t*n*f
- Average over *n* frames = avg*n*
- Hydrogen = voxelCount_OH

## Versions of software used

- NumPy 1.16.1
- TensorFlow 1.14
- Docker container for running GPU-enabled 3D CNN codes - atharvakelkar/tensorflow_1.14.0_keras_gpu
